subdomain     = "itca-eschool"

token_registry       = ""
user_registry        = ""
ci_registry          = "registry.gitlab.com"
ci_project_group     = "/itca/eschool/"
ci_project_name_fr   = "frontend"
ci_project_name_back = "backend"