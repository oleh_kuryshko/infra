variable "region" {}
variable "environment_name" {}
variable "environment_id" {}
variable "state_bucket" {}
variable "purpose" {}

variable "bastion_count" {
  default = "0"
}
variable "bastion_instance_type" {
  default = "t2.micro"
}
variable "bastion_key_name" {}
variable "my_ip" {}
variable "bastion_volume_size" {
  default = "10"
}
variable "bastion_user" {
  default = "ec2-user"
}
variable "bastion_key" {
  default = "/Users/okury/.ssh/aws-itca.pem"
}
variable "source_bastion_internal_key" {
  default = "/Users/okury/.ssh/itca"
}
variable "destination_bastion_internal_key" {
  default = "/home/ec2-user/.ssh/itca"
}
variable "ci_cd_count" {
  default = "0"
}
variable "ci_cd_instance_type" {
  default = "t2.micro"
}
variable "internal_key_name" {
  default = "itca-pub"
}
variable "internal_key_path" {
  default = "/Users/okury/.ssh/itca.pub"
}
variable "ci_cd_volume_size" {
  default = "10"
}

variable "gitlab_url" {
  default = "https://gitlab.com/"
}
variable "registration_token" {
  default = ""
}
variable "runner_tags_list" {
  default = "frontend, backend"
}
variable "runner_executor" {
  default = "shell"
}







